#include <iostream>
#include <string>
#include "DrawCanvas.h"


bool DrawCanvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    double Ah = this->get_height();
    double Aw = this->get_width();
    cr->rectangle(0, 0, Aw, Ah);
    cr->set_source_rgb(0.3, 0.3, 0.3);
    cr->fill_preserve();
    if(_dots.size()<2){
        return true;
    }
    //cr->add
    //dot_count()
    double Sx = _dots[0] + _Cx;
    double Sy = _dots[1] + _Cy;   
    cr->move_to(Sx, Sy);
    size_t i = 1;
    size_t i_max = _dots.size()/2;
    double DotX, DotY;
    while(i<i_max){
        DotX = _dots[i*2] + _Cx;
        DotY = _dots[i*2+1] + _Cy;
        cr->line_to(DotX, DotY);
        i++;
        
    }
    if(i==1){
        DotX = Sx;
        DotY = Sy;
    }
    //cr->line_to(200, 40);
    //cr->line_to(120, 160);
    //cr->line_to(40, 40);
    //cr->set_source_rgb(0.8, 0, 0.8);
    //cr->fill_preserve();
    cr->set_line_cap(Cairo::LINE_CAP_ROUND);
    cr->set_line_join(Cairo::LINE_JOIN_ROUND);
            
    //std::cout<<"redraw\n";
    cr->set_line_width(3);
    cr->set_source_rgb(0, 0, 0);    
    cr->stroke();
    cr->set_source_rgb(0.7, 0, 0);
    cr->move_to(DotX, DotY);
    cr->line_to(_Tx, _Ty);
    cr->stroke();
    return true;
}


DrawCanvas::DrawCanvas(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder):  Gtk::DrawingArea(cobject)
{

    add_events(Gdk::BUTTON_PRESS_MASK);
    add_events(Gdk::BUTTON_RELEASE_MASK);
    add_events(Gdk::POINTER_MOTION_MASK);
    add_events(Gdk::SCROLL_MASK);
}


void DrawCanvas::SetShape(shape_t shape)
{
    if (_curShape != shape) {
        _curShape = shape;
        /* Request re-drawing. */
        queue_draw();
    }
}

bool DrawCanvas::on_button_press_event(GdkEventButton *event){
    std::cout<<"button press evt\n";
    double Px = event->x;
    double Py = event->y;
    
    _dots.push_back(Px);
    _dots.push_back(Py);
    this->queue_draw();
    return false;
}

bool DrawCanvas::on_button_release_event(GdkEventButton *event){
    std::cout<<"button release evt\n";
    return false;
}


bool DrawCanvas::on_motion_notify_event(GdkEventMotion* event){
    if((pos_x)&&(pos_y)){
        pos_x->set_text(std::to_string(event->x));
        pos_y->set_text(std::to_string(event->y));
        
    }
    _Tx = event->x;
    _Ty = event->y;
    this->queue_draw();
    return false;
    //std::cout<<"mouse move evt\n";
}


bool DrawCanvas::on_scroll_event(GdkEventScroll* event){
    //std::cout<<"Scroll\n";
    this->queue_draw();
    switch(event->direction){
        case GDK_SCROLL_UP:
            _z += 1;
            std::cout<<"Scroll up\n";
            break;
        case GDK_SCROLL_DOWN:
            _z -= 1;
            std::cout<<"Scroll down\n";
            break;
        default:
            break;
    }
    return false;
}

void DrawCanvas::setLabelsXY(Gtk::Label * x, Gtk::Label * y){
    pos_x = x;
    pos_y = y;
}

void DrawCanvas::clear(){
    _dots.clear();
    this->queue_draw();
    //return true;
}

void DrawCanvas::setStatLabel(Gtk::Label * statLabel){
    _statLabel = statLabel;
}
