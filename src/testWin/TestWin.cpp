/* cppt-window.cpp
 *
 * Copyright 2018 Unknown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestWin.h"
#include <gtkmm/stylecontext.h>
#include <gtkmm/styleprovider.h>
#include <gtkmm/cssprovider.h>


TestWin::TestWin(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder):
        Gtk::Window(cobject), _builder(builder)
{

    //add_events(Gdk::POINTER_MOTION_MASK);
    Glib::RefPtr<Gtk::CssProvider> css = Gtk::CssProvider::create();
    css->load_from_resource("/org/gnome/GWT/TestWin.css");
    Glib::RefPtr<Gtk::StyleContext> sctx = Gtk::StyleContext::create();
    
    Glib::RefPtr<Gdk::Screen> screen;
    this->get_property("screen", screen);
    sctx->add_provider_for_screen(screen, css, 0);
    _builder->get_widget_derived("dr-area", _dcanv);
    Gtk::Label* pos_x,* pos_y;
    _builder->get_widget("stat-x", pos_x);
    _builder->get_widget("stat-y", pos_y);
    _dcanv->setLabelsXY(pos_x, pos_y);
    Gtk::Button * btn_clear;
    _builder->get_widget("btn-clear", btn_clear);
    btn_clear->signal_clicked().connect(sigc::mem_fun(*_dcanv, &DrawCanvas::clear));
    //signal_motion_notify_event().connect(sigc::mem_fun(*_dcanv, &DrawCanvas::on_my_motion_notify_event));
    /*Glib::RefPtr<Gtk::Action>::cast_dynamic(_builder->get_object("app.quit"))->
            signal_activate().connect(sigc::mem_fun(*this, &TestWin::onQuit));*/
	//builder = Gtk::Builder::create_from_resource("/org/gnome/GWT/TestWin.glade");
	
	
	//builder->get_widget("headerbar", headerbar);
	//builder->get_widget("lbl", label);
	//builder->get_widget("utton", button);
	//add(*label);
	//add(*button);
	
	//label->show();
	//button->show();
	//set_titlebar(*headerbar);
	//headerbar->show();
}


void TestWin::onQuit(){
    hide();
}
