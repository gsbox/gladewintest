#pragma once


#include <vector>
#include <gtkmm/drawingarea.h>
#include <gtkmm/builder.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/label.h>
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <cairomm/cairomm.h>




class DrawCanvas : public Gtk::DrawingArea {
  public:
  
  typedef enum {
    SHAPE_RECTANGLE,
    SHAPE_ELLIPSE,
    SHAPE_TRIANGLE
    
  } shape_t;
  
  typedef enum {
    WM_DRAW,
    WM_MOVE
  } work_mode_t;

  private:
  
  double _z = 0;
  double _Cx = 0;
  double _Cy = 0;
  
  double _Tx = 0;
  double _Ty = 0;
  
  std::vector<double> _dots;
  
  Gtk::Label * pos_x=nullptr, * pos_y=nullptr;
  Gtk::Label * _statLabel = nullptr;
  shape_t _curShape = SHAPE_RECTANGLE;

  /** Drawing event handler. */
  
  virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
  
  virtual bool on_button_press_event(GdkEventButton *event);
  virtual bool on_button_release_event(GdkEventButton *event);
  
  virtual bool on_motion_notify_event(GdkEventMotion* event);
  virtual bool on_scroll_event(GdkEventScroll* event);
  
  
  public:
  
  DrawCanvas(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder);
  void SetShape(shape_t shape);
  void setLabelsXY(Gtk::Label * x, Gtk::Label * y);
  void setStatLabel(Gtk::Label * statLabel);
  void clear();
};
